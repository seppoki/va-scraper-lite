import { Vacancy } from "../vacancy";
import { scrape as tepalvelut } from "../sources/te-palvelut";
import { scrape as valtiolle } from "../sources/valtiolle";
import { scrape as kuntarekry } from "../sources/kuntarekry";
// import { scrape as rekrytointi } from "../sources/rekrytointi";
import { scrape as duunitori } from "../sources/duunitori";
import { scrape as linkedin } from "../sources/linkedin";
import { scrape as monster } from "../sources/monster";
import { scrape as tiitus } from "../sources/tiitus";

export const sources: (() => Promise<Array<Vacancy>>)[] = [];
sources.push(tepalvelut);
sources.push(valtiolle);
sources.push(kuntarekry);
// sources.push(rekrytointi);
sources.push(duunitori);
sources.push(linkedin);
sources.push(monster);
sources.push(tiitus);
