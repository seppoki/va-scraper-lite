import cheerio from "cheerio";
import JSON5 from "json5";
import { getUnparsed } from "../database";
import { logger } from "../logger";
import { delay } from "../utils";
import { Vacancy } from "../vacancy";

import got from "got";
import Ajv, { JTDSchemaType } from "ajv/dist/jtd";
const ajv = new Ajv();

interface Doc {
  id: string;
  name: string;
}
const docsSchema: JTDSchemaType<Array<Doc>> = {
  elements: {
    properties: {
      id: { type: "string" },
      name: { type: "string" },
    },
    additionalProperties: true,
  },
};

const validateDocs = ajv.compile(docsSchema);

const listUrl = "https://duunitori.fi/tyopaikat?order_by=date_posted";

const getUrls = async (page: number): Promise<Array<string>> => {
  const response = await got(listUrl + `&page=${page}`);
  const bulk = response.body.replace(/\s/g, "");
  const arr = bulk.match(/impressions:(.*?)}}\);/);
  const urls: Array<string> = [];
  if (arr && arr.length > 1) {
    const docs = JSON5.parse(arr[1]);
    if (validateDocs(docs)) {
      for (const doc of docs) urls.push("https://duunitori.fi/tyopaikat/tyo/" + doc.id);
    } else {
      logger.error(validateDocs.errors);
    }
  }
  return urls;
};

export const scrape = async (): Promise<Array<Vacancy>> => {
  const vacancies: Array<Vacancy> = [];
  let urls: Array<string> = [];
  for (let page = 1; page < 50; page++) {
    await delay(500);
    try {
      const pageurls = await getUrls(page);
      const unparsed = await getUnparsed(pageurls);
      if (unparsed.length == 0) break;
      urls = [...urls, ...unparsed];
    } catch (err) {
      logger.error(err);
    }
  }
  logger.info("duunitori has %d unparsed urls", urls.length);
  for (const url of urls) {
    await delay(500);
    try {
      const response = await got(url);
      const $ = cheerio.load(response.body);
      const header = $("h1.text--break-word").text();
      const contents = $("div.description-box").text();
      const source = "Duunitori";
      if (header.length > 0 && contents.length > 0) {
        const vacancy = new Vacancy(source, header, contents, url);
        vacancies.push(vacancy);
      }
    } catch (err) {
      logger.error(err);
    }
  }
  return vacancies;
};
