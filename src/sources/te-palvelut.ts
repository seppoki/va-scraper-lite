import { logger } from "../logger";
import { Vacancy } from "../vacancy";

const listUrl =
  "https://paikat.te-palvelut.fi/tpt-api/v1/tyopaikat?ilmoitettuPvm=2&vuokrapaikka=---&kentat=ilmoitusnumero,otsikko,kuvausteksti&start&start=0&ss=true&facet.fkentat=hakuTyoaikakoodi,ammattikoodi,aluehaku,hakuTyonKestoKoodi,hakuTyosuhdetyyppikoodi,oppisopimus&facet.fsort=index&facet.flimit=-1";

import got from "got";
import Ajv, { JTDSchemaType } from "ajv/dist/jtd";
const ajv = new Ajv();

interface CompleteDoc {
  ilmoitusnumero: number;
  otsikko: string;
  kuvausteksti: string;
}
interface PartialDoc {
  ilmoitusnumero: number | undefined;
  otsikko: string | undefined;
  kuvausteksti: string | undefined;
}
interface Response {
  response: {
    numFound: number;
    docs: Array<PartialDoc>;
  };
}
const responseSchema: JTDSchemaType<Response> = {
  properties: {
    response: {
      properties: {
        numFound: { type: "int32" },
        docs: {
          elements: {
            optionalProperties: {
              ilmoitusnumero: { type: "int32" },
              otsikko: { type: "string" },
              kuvausteksti: { type: "string" },
            },
          },
        },
      },
      additionalProperties: true,
    },
  },
  additionalProperties: true,
};

const validateResponse = ajv.compile(responseSchema);

const docSchema: JTDSchemaType<CompleteDoc> = {
  properties: {
    ilmoitusnumero: { type: "int32" },
    otsikko: { type: "string" },
    kuvausteksti: { type: "string" },
  },
};
const validateDoc = ajv.compile(docSchema);

export const scrape = async (): Promise<Array<Vacancy>> => {
  const vacancies: Array<Vacancy> = [];
  return got(listUrl)
    .json()
    .then(response => {
      if (validateResponse(response)) {
        for (const doc of response.response.docs) {
          if (validateDoc(doc)) {
            const vacancy = new Vacancy(
              "TE-palvelut",
              doc.otsikko,
              doc.kuvausteksti,
              "https://paikat.te-palvelut.fi/tpt/" + doc.ilmoitusnumero
            );
            vacancies.push(vacancy);
          } else {
            logger.debug("doc failed validation");
          }
        }
        return vacancies;
      } else {
        logger.info("response failed validation");
        return vacancies;
      }
    })
    .catch(err => {
      logger.error(err);
      return vacancies;
    });
};
