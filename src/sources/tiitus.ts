import { logger } from "../logger";
import { Vacancy } from "../vacancy";

const listUrl = "https://secure.tiitus.fi/api/market_jobs/?region_types=1&limit=60&regions_sectors=1";

import got from "got";
import Ajv, { JTDSchemaType } from "ajv/dist/jtd";
const ajv = new Ajv();

interface CompleteDoc {
  title: string;
  description: string;
  market_url: string;
}
interface PartialDoc {
  title: string | undefined | null;
  description: string | undefined | null;
  market_url: string | undefined | null;
}
interface ResponseBody {
  results: Array<PartialDoc>;
}
const responseSchema: JTDSchemaType<ResponseBody> = {
  properties: {
    results: {
      elements: {
        optionalProperties: {
          title: { type: "string", nullable: true },
          description: { type: "string", nullable: true },
          market_url: { type: "string", nullable: true },
        },
        additionalProperties: true,
      },
    },
  },
  additionalProperties: true,
};

const validateResponse = ajv.compile(responseSchema);

const docSchema: JTDSchemaType<CompleteDoc> = {
  properties: {
    title: { type: "string" },
    description: { type: "string" },
    market_url: { type: "string" },
  },
  additionalProperties: true,
};
const validateDoc = ajv.compile(docSchema);

export const scrape = async (): Promise<Array<Vacancy>> => {
  const vacancies: Array<Vacancy> = [];
  return got(listUrl)
    .json()
    .then(response => {
      if (validateResponse(response)) {
        for (const doc of response.results) {
          if (validateDoc(doc)) {
            const vacancy = new Vacancy("Tiitus", doc.title, doc.description, doc.market_url);
            vacancies.push(vacancy);
          } else {
            // logger.info(validateDoc.errors);
          }
        }
        return vacancies;
      } else {
        logger.info(validateResponse.errors);
        return vacancies;
      }
    })
    .catch(err => {
      logger.error(err);
      return vacancies;
    });
};
