import cheerio from "cheerio";
import { getUnparsed } from "../database";
import { delay } from "../utils";
import { logger } from "../logger";
import { Vacancy } from "../vacancy";

import got from "got";

const listUrl =
  "https://fi.linkedin.com/jobs-guest/jobs/api/seeMoreJobPostings/search?location=Finland&geoId=100456013&trk=public_jobs_jobs-search-bar_search-submit&sortBy=DD&f_TPR=r86400";

const getUrls = async (start: number): Promise<Array<string>> => {
  const response = await got(listUrl + `&start=${start}`);
  const $ = cheerio.load(response.body);
  const urls: Array<string> = [];
  $("a").each((_, val) => {
    const url = $(val).attr("href");
    if (url) {
      const match = url.match(/-(\d*)\?refId/);
      if (match) {
        urls.push(`http://fi.linkedin.com/jobs/view/${match[1]}`);
      }
    }
  });
  return urls;
};

export const scrape = async (): Promise<Array<Vacancy>> => {
  const vacancies: Array<Vacancy> = [];
  let urls: Array<string> = [];
  for (let start = 0; start < 1000; start += 25) {
    await delay(500);
    try {
      const pageurls = await getUrls(start);
      if (pageurls.length == 0) break;
      const unparsed = await getUnparsed(pageurls);
      if (unparsed.length == 0) break;
      urls = [...urls, ...unparsed];
    } catch (err) {
      logger.error(err);
    }
  }
  logger.info("linkedin has %d unparsed urls", urls.length);
  for (const url of urls) {
    await delay(500);
    try {
      const response = await got(url);
      const $ = cheerio.load(response.body);
      const header = $("h1").text();
      const contents = $("section > div.show-more-less-html__markup").text();
      const source = "Linkedin";
      if (header.length > 0 && contents.length > 0) {
        const vacancy = new Vacancy(source, header, contents, url);
        vacancies.push(vacancy);
      }
    } catch (err) {
      logger.error(err);
    }
  }
  return vacancies;
};
