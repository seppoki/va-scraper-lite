import { logger } from "../logger";
import { Vacancy } from "../vacancy";

const listUrl = "https://www.valtiolle.fi/api/Valtiolle/VacancySearch?languageid=1";

import got from "got";
import Ajv, { JTDSchemaType } from "ajv/dist/jtd";
const ajv = new Ajv();

interface CompleteDoc {
  AssignmentTitle: string;
  SearchContent: string;
  RefNo: string;
}
interface PartialDoc {
  AssignmentTitle: string | undefined;
  SearchContent: string | undefined;
  RefNo: string | undefined;
}
const responseSchema: JTDSchemaType<Array<PartialDoc>> = {
  elements: {
    optionalProperties: {
      AssignmentTitle: { type: "string" },
      SearchContent: { type: "string" },
      RefNo: { type: "string" },
    },
    additionalProperties: true,
  },
};

const validateResponse = ajv.compile(responseSchema);

const docSchema: JTDSchemaType<CompleteDoc> = {
  properties: {
    AssignmentTitle: { type: "string" },
    SearchContent: { type: "string" },
    RefNo: { type: "string" },
  },
  additionalProperties: true,
};
const validateDoc = ajv.compile(docSchema);

export const scrape = async (): Promise<Array<Vacancy>> => {
  const vacancies: Array<Vacancy> = [];
  return got(listUrl)
    .json()
    .then(response => {
      if (validateResponse(response)) {
        for (const doc of response) {
          if (validateDoc(doc)) {
            const vacancy = new Vacancy(
              "Valtiolle",
              doc.AssignmentTitle,
              doc.SearchContent,
              "https://www.valtiolle.fi/fi-FI/ilmoitus?id=" + doc.RefNo
            );
            vacancies.push(vacancy);
          } else {
            logger.info(validateDoc.errors);
          }
        }
        return vacancies;
      } else {
        logger.info(validateResponse.errors);
        return vacancies;
      }
    })
    .catch(err => {
      logger.error(err);
      return vacancies;
    });
};
