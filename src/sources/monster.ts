import cheerio from "cheerio";
import got from "got";
import { getUnparsed } from "../database";
import { delay } from "../utils";
import { logger } from "../logger";
import { Vacancy } from "../vacancy";

const listUrl = "https://www.monster.fi/tyopaikat";

const getUrls = async (start: number): Promise<Array<string>> => {
  const urls: Array<string> = [];
  try {
    const response = await got(listUrl + `?page=${start}`);
    const $ = cheerio.load(response.body);
    $("h2 > a").each((_, val) => {
      const url = $(val).attr("href");
      if (url) urls.push(url);
    });
  } catch (err) {
    logger.error(err);
    return urls;
  }
  return urls;
};

export const scrape = async (): Promise<Array<Vacancy>> => {
  const vacancies: Array<Vacancy> = [];
  let urls: Array<string> = [];
  for (let page = 1; page < 20; page++) {
    await delay(500);
    try {
      const pageurls = await getUrls(page);
      if (pageurls.length == 0) break;
      const unparsed = await getUnparsed(pageurls);
      if (unparsed.length == 0) break;
      urls = [...urls, ...unparsed];
    } catch (err) {
      logger.error(err);
    }
  }
  logger.info("monster has %d unparsed urls", urls.length);
  for (const url of urls) {
    await delay(500);
    try {
      const response = await got(url);
      const $ = cheerio.load(response.body);
      const header = $("h1").text();
      const contents = $("div.content").text();
      const source = "Monster";
      if (header.length > 0 && contents.length > 0) {
        const vacancy = new Vacancy(source, header, contents, url);
        vacancies.push(vacancy);
      }
    } catch (err) {
      logger.error(err);
    }
  }
  return vacancies;
};
