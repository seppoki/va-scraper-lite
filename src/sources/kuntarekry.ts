import cheerio from "cheerio";
import { logger } from "../logger";
import { getUnparsed } from "../database";
import { delay } from "../utils";
import { Vacancy } from "../vacancy";

const listUrl =
  "https://www.kuntarekry.fi/fi/tyopaikat/?lang=fi_FI%2Csv_SE%2Cen_US&sort=%22-changetime%22&limit=200&display=list";

import got from "got";

export const scrape = async (): Promise<Array<Vacancy>> => {
  const vacancies: Array<Vacancy> = [];
  const response = await got(listUrl);
  const $ = cheerio.load(response.body);
  let urls: Array<string> = [];
  $("article.job-list-item > div > div > a[href]").each((_, val) => {
    urls.push("https://www.kuntarekry.fi" + $(val).attr("href"));
  });
  if (urls.length == 0) return vacancies;
  urls = await getUnparsed(urls);
  logger.info("kuntarekry has %d unparsed urls", urls.length);
  for (const url of urls) {
    await delay(500);
    try {
      const response = await got(url);
      const $ = cheerio.load(response.body);
      const header = $(".aside-title").text();
      const contentsList: Array<string> = [];
      $("div.panel > p").each((_, val) => {
        contentsList.push($(val).text());
      });
      const contents = contentsList.join(" ");
      const source = "Kuntarekry";
      if (header.length > 0 && contents.length > 0) {
        const vacancy = new Vacancy(source, header, contents, url);
        vacancies.push(vacancy);
      }
    } catch (err) {
      logger.error(err);
    }
  }
  return vacancies;
};
