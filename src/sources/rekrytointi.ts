import cheerio from "cheerio";
import { getUnparsed } from "../database";
import { logger } from "../logger";
import { delay } from "../utils";
import { Vacancy } from "../vacancy";

const listUrl = "https://rekrytointi.com/wp-content/themes/jobify-child/ajax.php?wpml_lang=fi";

import got from "got";
import FormData from "form-data";
import Ajv, { JTDSchemaType } from "ajv/dist/jtd";
const ajv = new Ajv();

interface Response {
  html: string;
}
const responseSchema: JTDSchemaType<Response> = {
  properties: {
    html: { type: "string" },
  },
  additionalProperties: true,
};

const validateResponse = ajv.compile(responseSchema);

export const scrape = async (): Promise<Array<Vacancy>> => {
  const vacancies: Array<Vacancy> = [];
  const form = new FormData();
  form.append("per_page", 200);
  form.append("action", "get_listings");

  try {
    const response = await got.post(listUrl, { body: form }).json();

    if (!validateResponse(response)) return vacancies;

    let urls: Array<string> = [];
    const $ = cheerio.load(response.html);
    $("li.job_listing > a[href]").each((_, val) => {
      const url = $(val).attr("href");
      if (url) urls.push(url);
    });
    if (urls.length == 0) return vacancies;
    urls = await getUnparsed(urls);
    logger.info("rekrytointi has %d unparsed urls", urls.length);
    for (const url of urls) {
      await delay(500);
      try {
        const response = await got(url);
        const $ = cheerio.load(response.body);
        const header = $("h1.page-title").text();
        let contents = $("div#content > div").text();
        contents = contents.replace(/\nOsaamisalue: (.|\s)*/gm, "");
        const source = "Rekrytointi";
        if (header.length > 0 && contents.length > 0) {
          const vacancy = new Vacancy(source, header, contents, url);
          vacancies.push(vacancy);
        }
      } catch (err) {
        logger.error(err);
        continue;
      }
    }
  } catch (err) {
    logger.error(err);
    return vacancies;
  }
  return vacancies;
};
