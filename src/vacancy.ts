import { clean, language } from "./utils";

export class Vacancy {
  source: string;
  header: string;
  contents: string;
  url: string;
  lang: string;

  constructor(source: string, header: string, contents: string, url: string) {
    this.source = source;
    this.header = header.trim();
    contents = clean(contents)
    this.contents = contents;
    this.url = url;
    this.lang = language(contents);
  }
}
