import pgPromise from "pg-promise";
import { logger } from "./logger";

const host = process.env.DB_HOST;
const port = parseInt(<string>process.env.DB_PORT, 10);
const database = process.env.DB_DATABASE;
const user = process.env.DB_USER;
const password = process.env.DB_PASSWORD;

if (!host || !port || !database || !user || !password) {
  logger.error("missing env variables");
  process.exit(1);
}

const pgp = pgPromise();
const db = pgp({ host, port, database, user, password });

const insertcs = new pgp.helpers.ColumnSet(["url", "header", "contents", "source", "lang"], { table: "vacancies" });

export const insert = async (
  entries: Array<{ url: string; header: string; contents: string; source: string, lang: string }>
): Promise<null> => {
  const query = pgp.helpers.insert(entries, insertcs) + " ON CONFLICT DO NOTHING";
  return db.none(query);
};

export const getUnparsed = async (urls: Array<string>): Promise<Array<string>> => {
  const keyed = urls.map(url => ({ url: url }));
  const qvalues = pgp.helpers.values(keyed, ["url"]);
  const query = pgp.as.format(
    `
    SELECT urls.url
    FROM (VALUES $1:raw) AS urls (url)
    LEFT JOIN vacancies ON urls.url = vacancies.url
    WHERE vacancies.url IS NULL
    `,
    qvalues
  );
  return db.any(query).then(res => res.map(url => url.url));
};
