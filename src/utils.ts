import franc from "franc";

export const delay = async (ms: number): Promise<null> => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

export const clean = (str: string): string => {
  return (
    str
      //replace all non-word characters
      .replace(/[^a-zA-Z0-9_öÖäÄåÅ]/gm, " ")
      //replace 1 character words
      .replace(/(\s\S)+\s/gm, " ")
      //replace only-digit words
      .replace(/(\s\d+)+\s/gm, " ")
      //remove consecutive spaces
      .replace(/\s\s+/gm, " ")
      .trim()
      .toLowerCase()
  );
};

export const language = (str: string): string => {
  return franc(str, { only: ["eng", "fin", "swe"] });
};
