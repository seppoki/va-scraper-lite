import dotenv from "dotenv";
dotenv.config();
import { logger } from "./logger";
import { insert } from "./database";
import { Vacancy } from "./vacancy";

import { sources } from "./sources/sources";

const run = async () => {
  logger.trace("log level is: %s", process.env.LOG_LEVEL);
  for (const source of sources) {
    const vacancies: Array<Vacancy> = await source();
    if (vacancies.length > 0) {
      logger.info("inserting " + vacancies.length);
      insert(vacancies)
        .then()
        .catch(err => logger.error(err));
    }
  }
};

run().catch(err => logger.error(err));
