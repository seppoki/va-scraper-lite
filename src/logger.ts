import tracer from "tracer";
export const logger = tracer.colorConsole({ level: process.env.LOG_LEVEL });
